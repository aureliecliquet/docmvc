## ORGANISER SON CODE SELON L'ARCHITECTURE MVC

## Qu'est-ce que l'architecture MVC ?

Un des plus célèbres design patterns s'appelle MVC, qui signifie Modèle - Vue - Contrôleur. 

Le pattern MVC permet de bien organiser son code source. 
Il va vous aider à savoir quels fichiers créer, mais surtout à définir leur rôle. 
Le but de MVC est justement de séparer la logique du code en trois parties que l'on retrouve dans des fichiers distincts, comme l'explique la description qui suit.

Modèle : cette partie gère les données de votre site. 
Son rôle est d'aller récupérer les informations « brutes » dans la base de données, de les organiser et de les assembler pour qu'elles puissent ensuite être traitées par le contrôleur. 
On y trouve donc les requêtes SQL.

"	Parfois, les données ne sont pas stockées dans une base de données. C'est plus rare, mais on peut être amené à aller chercher des données dans des fichiers. Dans ce cas, le rôle du modèle est de faire les opérations d'ouverture, de lecture et d'écriture de fichiers (fonctions fopen, fgets, etc.).	"

<!-- slides 2 -->

Vue : cette partie se concentre sur l'affichage. 
Elle ne fait presque aucun calcul et se contente de récupérer des variables pour savoir ce qu'elle doit afficher. 
On y trouve essentiellement du code HTML mais aussi quelques boucles et conditions PHP très simples, pour afficher par exemple la liste des messages des forums.

Contrôleur : cette partie gère la logique du code qui prend des décisions. 
C'est en quelque sorte l'intermédiaire entre le modèle et la vue : le contrôleur va demander au modèle les données, les analyser, prendre des décisions et renvoyer le texte à afficher à la vue. 
Le contrôleur contient exclusivement du PHP. C'est notamment lui qui détermine si le visiteur a le droit de voir la page ou non (gestion des droits d'accès).

Il faut tout d'abord retenir que le contrôleur est le chef d'orchestre : c'est lui qui reçoit la requête du visiteur et qui contacte d'autres fichiers (le modèle et la vue) pour échanger des informations avec eux.

Le fichier du contrôleur demande les données au modèle sans se soucier de la façon dont celui-ci va les récupérer. 
Par exemple : « Donne-moi la liste des 30 derniers messages du forum no 5 ». 
Le modèle traduit cette demande en une requête SQL, récupère les informations et les renvoie au contrôleur.

Une fois les données récupérées, le contrôleur les transmet à la vue qui se chargera d'afficher la liste des messages.

Dans les cas les plus simples, le rôle du contrôleur ne se limite pas à cela : s'il y a des calculs ou des vérifications d'autorisations à faire, des images à miniaturiser, c'est lui qui s'en chargera.

<!-- slides 3 --> 

Concrètement, le visiteur demandera la page au contrôleur et c'est la vue qui lui sera retournée. 
Bien entendu, tout cela est transparent pour lui, il ne voit pas tout ce qui se passe sur le serveur. 
C'est un schéma plus complexe que ce à quoi vous avez été habitués, bien évidemment : c'est pourtant sur ce type d'architecture que repose un très grand nombre de sites professionnels !

## DANS LA PRATIQUE : 

Il y aura probablement plusieurs fichiers au lieu d'un seul, mais au final le code sera beaucoup plus clair et mieux découpé.
L'intérêt de respecter l'architecture MVC ne se voit pas forcément de suite. 
En revanche, si vous améliorez votre site plus tard, vous serez bien heureux d'avoir pris la peine de vous organiser avant ! De plus, si vous travaillez à plusieurs, cela vous permet de séparer les tâches : une personne peut s'occuper du contrôleur, une autre de la vue et la dernière du modèle. 
Si vous vous mettez d'accord au préalable sur les noms des variables et fonctions à appeler entre les fichiers, vous pouvez travailler ensemble en parallèle et avancer ainsi beaucoup plus vite !

D'ailleurs, la théorie « pure » de MVC est bien souvent inapplicable en pratique. Il faut faire des concessions, savoir être pragmatique : on prend les bonnes idées et on met de côté celles qui se révèlent trop contraignantes.

À la racine de votre site, je vous propose de créer trois répertoires :

modele ;
vue ;
controleur.

Dans chacun d'eux, vous pouvez créer un sous-répertoire pour chaque « module » de votre site.

Pour le moment, créez par exemple un répertoire blog dans chacun de ces dossiers. On aura ainsi l'architecture suivante :

modele/blog : contient les fichiers gérant l'accès à la base de données du blog ;
vue/blog : contient les fichiers gérant l'affichage du blog ;
controleur/blog : contient les fichiers contrôlant le fonctionnement global du blog.

On peut commencer par travailler sur l'élément que l'on veut ; il n'y a pas d'ordre particulier à suivre (comme je vous le disais, on peut travailler à trois en parallèle sur chacun de ces éléments).

## LE MODELE : 

<!-- slides 4 -->

Créez un fichier php dans modele/blog. Ce fichier contiendra une fonction dont le rôle sera de retourner un certain nombre d'éléments depuis la base de données. C'est tout ce qu'elle fera.

Le code source ne contient pas de réelles nouveautés. Il s'agit d'une fonction qui prend en paramètre un offset et une limite. Elle retourne le nombre de billets demandés à partir du billet nooffset. Ces paramètres sont transmis à MySQL via une requête préparée.

la fonction bindParam qui permet de préciser que le paramètre est un entier (PDO$\colon\colon$PARAM_INT). Cette méthode alternative est obligatoire dans le cas où les paramètres sont situés dans la clause LIMIT car il faut préciser qu'il s'agit d'entiers.

La connexion à la base de données aura été faite précédemment.
Cela nous évite d'avoir à recréer une connexion à la base de données dans chaque fonction, ce qui serait très mauvais pour les performances du site.
Plutôt que de faire une boucle de fetch(), on appelle ici la fonction fetchAll() qui assemble toutes les données dans un grand array. La fonction retourne donc un array contenant les billets demandés.

Ce fichier PHP ne contient pas la balise de fermeture ?>. Celle-ci n'est en effet pas obligatoire...

## LE CONTROLEUR : 

<!-- slides 5 -->

Le contrôleur est le chef d'orchestre de notre application. 
Il demande au modèle les données, les traite et appelle la vue qui utilisera ces données pour afficher la page.

Dans controleur/blog, créez par exemple un fichier index.php qui représentera la page d'accueil du blog.

On retrouve un cheminement simple. 
Le rôle de la page d'accueil du blog est d'afficher les cinq derniers éléments. On appelle donc la fonction get_billets() du modèle, on récupère la liste « brute » de ces billets que l'on traite dans un foreach pour protéger l'affichage avec htmlspecialchars() et créer les retours à la ligne du contenu avec nl2br().
S'il y avait d'autres opérations à faire avant l'appel de la vue, comme la gestion des droits d'accès, ce serait le bon moment. 
En l'occurrence, il n'est pas nécessaire d'effectuer d'autres opérations dans le cas présent.

La présence des fonctions de sécurisation des données dans le contrôleur est discutable. 
On pourrait laisser cette tâche à la vue, qui ferait donc les htmlspecialchars, ou bien à une couche intermédiaire entre le contrôleur et la vue (c'est d'ailleurs ce que proposent certains frameworks). 
Il n'y a pas une seule bonne approche mais plusieurs, chacune ayant ses avantages et inconvénients.

Ce qui est intéressant, c'est que la fonction get_billets() pourra être réutilisée à d'autres occasions. 
Ici, on l'appelle toujours avec les mêmes paramètres, mais on pourrait en avoir besoin dans d'autres contrôleurs. 
On pourrait aussi améliorer ce contrôleur-ci pour gérer la pagination des billets du blog et afficher un nombre différent de billets par page en fonction des préférences du visiteur.

## LA VUE : 

<!-- slides 6 -->

Il ne nous reste plus qu'à créer la vue correspondant à la page d'accueil des derniers éléments. 
Ce sera très simple : le fichier de la vue devra simplement afficher le contenu de l'array $billets sans se soucier de la sécurité ou des requêtes SQL. 
Tout aura été préparé avant.

Vous pouvez par exemple créer un fichier index.php dans vue/blog.

Ce code source contient essentiellement du HTML et quelques morceaux de PHP pour afficher le contenu des variables et effectuer la boucle nécessaire.

L'intérêt est que ce fichier est débarrassé de toute « logique » du code : vous pouvez aisément le donner à un spécialiste de la mise en page web pour qu'il améliore la présentation. 
Celui-ci n'aura pas besoin de connaître le PHP pour travailler sur la mise en page du blog. 
Il suffit de lui expliquer le principe de la boucle et comment on affiche une variable, c'est vraiment peu de choses.

## LE CONTROLEUR GLOBAL DU BLOG

<!-- slides 7 -->

Bien qu'en théorie ce ne soit pas obligatoire, créer un fichier contrôleur « global » par module à la racine de votre site. 
Son rôle sera essentiellement de traiter les paramètres $_GET et d'appeler le contrôleur correspondant en fonction de la page demandée. 
On peut aussi profiter de ce point « central » pour créer la connexion à la base de données.
J'ai ici choisi d'inclure un fichier connexion_sql.php qui crée l'objet $bdd. 
Ce fichier pourra ainsi être partagé entre les différents modules de mon site.

Vous pouvez par exemple, créer ce fichier blog.php à la racine de votre site.

L'avantage de cette page est aussi qu'elle permet de masquer l'architecture de votre site au visiteur. 
Sans elle, ce dernier aurait dû aller sur la page controleur/blog/index.php pour afficher votre blog. 
Cela aurait pu marcher, mais aurait probablement créé de la confusion chez vos visiteurs, en plus de complexifier inutilement l'URL.
Pour accéder à la page d'accueil du blog, il suffit maintenant d'ouvrir la page blog.php !

## RESUME DES FICHIERS CREES 

<!-- Slides 8 --> 

## LES FRAMEWORKS MVC

Voici quelques frameworks PHP célèbres qu'il faut connaître :

CodeIgniter ;
CakePHP ;
Symfony ;
Jelix ;
Zend Framework.

Ils sont tous basés sur une architecture MVC et proposent en outre de nombreux outils pour faciliter le développement de son site web.

## EN RESUME 

MVC est un design pattern, une bonne pratique de programmation qui recommande de découper son code en trois parties qui gèrent les éléments suivants :

Modèle : stockage des données ;
Vue : affichage de la page ;
Contrôleur : logique, calculs et décisions.

Utiliser l'architecture MVC sur son site web en PHP est recommandé, car cela permet d'avoir un code plus facile à maintenir et à faire évoluer.

De nombreux frameworks PHP, tels que Symfony et le Zend Framework, vous permettent de mettre rapidement en place les bases d'une architecture MVC sur votre site. 
Ce sont des outils appréciés des professionnels qui nécessitent cependant un certain temps d'adaptation.

Support de présentation : http://slides.com/aurelieslide/mvc